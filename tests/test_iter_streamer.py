import os

import itertools

from parameterized import parameterized

from dicomraw.archive import IterStreamer
import unittest
import random


class TestIterStreamer(unittest.TestCase):

    @parameterized.expand(
        [x for x in itertools.product(
            [10, 18, 29, 100, 102],  # buffer_size
            [5, 10, 11, 99, 100, 101],  # read_chunk_size
            [4, 5, 6, 200],  # iterator_chunk_size
            [5000, 200]  # input_string_size
        )],
        testcase_func_name=lambda testcase_func, param_num, param:
        "%s_buffersize_%d_readsize_%d_itersize_%d_stringsize_%d" % (
                testcase_func.__name__, param[0][0], param[0][1], param[0][2], param[0][3])
    )
    def test_buffer(self, buffer_size, read_chunk_size, iterator_chunk_size, input_string_size):

        random.seed(20170327)
        random_reads = itertools.cycle([random.randint(0, 40) for _ in range(20)])
        input_string = os.urandom(input_string_size)

        def break_into_fragments(seq, size):
            return (seq[pos_:pos_ + size] for pos_ in range(0, len(seq), size))

        stream = IterStreamer(break_into_fragments(input_string, iterator_chunk_size), buffer_size=buffer_size)

        output_string = b''
        while True:
            data = stream.read(read_chunk_size)
            if len(data) == 0:
                break
            output_string += data
            # test seek/tell functionality
            pos = stream.tell()
            y = stream.read(next(random_reads))
            stream.seek(stream.tell()-len(y))
            pos2 = stream.tell()
            assert pos == pos2

        assert output_string == input_string
