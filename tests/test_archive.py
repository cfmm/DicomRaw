import io
import os
import unittest
import itertools
from parameterized import parameterized

import zipfile
from tempfile import NamedTemporaryFile

import dicomraw


class RandomData(object):
    def __init__(self, numbytes):
        self._data = io.BytesIO(os.urandom(numbytes))
        self._buffer_size = 2**20
        self._numbytes = numbytes

    def __repr__(self):
        return "random_{0}_bytes".format(self._numbytes)

    def iter(self):
        return self.__iter__()

    def __iter__(self):
        self._data.seek(0)
        while True:
            data = self._data.read(self._buffer_size)
            if len(data) == 0:
                break
            yield data


class TestStreamingArchive(unittest.TestCase):

    # noinspection PyPep8Naming
    def setUp(self):
        self._temp_dir = os.path.abspath('tests/data/tmp')
        if not os.path.exists(self._temp_dir):
            os.makedirs(self._temp_dir)

    @staticmethod
    def stream_compress_file(input_paths, archive_name, archive_fp, compress_type=zipfile.ZIP_DEFLATED,
                             buffer_size=2 ** 20):
        archive = dicomraw.StreamingArchive(input_paths, archive_name=archive_name, compress_type=compress_type)
        compressed_stream = archive.get_compressed_stream()

        while True:
            data = compressed_stream.read(buffer_size)
            if len(data) == 0:
                break
            archive_fp.write(data)
        archive_fp.flush()

    @parameterized.expand(itertools.product(
        [
            'tests/data/noise_phantom/930.dcm',
            'tests/data/noise_phantom',
            RandomData(2**22-7)
        ],
        [zipfile.ZIP_DEFLATED, zipfile.ZIP_STORED])
    )
    def test_streaming_compress(self, input_data, compress_type):
        contents = []
        if isinstance(input_data, str):
            path = os.path.abspath(input_data)
            if os.path.isdir(path):
                for root, _, files in os.walk(input_data):
                    for name in files:
                        fpath = root + os.sep + name
                        with open(fpath, 'rb') as fp:
                            contents.append(fp.read())
            elif os.path.isfile(path):
                with open(path, 'rb') as fp:
                    contents += [fp.read()]
            else:
                raise RuntimeError("String input_data is neither a valid file nor a directory")
        else:
            contents = [b''.join(d for d in input_data.iter())]
            input_data = input_data.iter()
        with NamedTemporaryFile(dir=self._temp_dir, suffix=".zip") as streamed_archive_output_file:
            self.stream_compress_file(input_data, "arc", streamed_archive_output_file, compress_type)
            assert os.path.exists(streamed_archive_output_file.name)
            assert zipfile.is_zipfile(streamed_archive_output_file.name)
            streamed_archive_from_disk = zipfile.ZipFile(streamed_archive_output_file.name)
            assert len(streamed_archive_from_disk.namelist()) == len(contents)

            for i in range(len(contents)):
                contents_streamed = streamed_archive_from_disk.read(streamed_archive_from_disk.namelist()[i])
                assert contents[i] == contents_streamed

    def test_directory_with_subdirs(self):
        with NamedTemporaryFile(dir=self._temp_dir, suffix=".zip") as streamed_archive_output_file:
            self.stream_compress_file(
                os.path.join(os.path.abspath("tests/data"), "random_files"),
                "random_files",
                streamed_archive_output_file
            )
            streamed_archive_from_disk = zipfile.ZipFile(streamed_archive_output_file.name)
            assert all(x in streamed_archive_from_disk.namelist() for x in [
                'random_files/file1',
                'random_files/subdir1/file2',
                'random_files/subdir2/file3',
                'random_files/subdir2/file4'
            ])

    def test_multiple_files(self):
        with NamedTemporaryFile(dir=self._temp_dir, suffix=".zip") as streamed_archive_output_file:
            self.stream_compress_file(
                [
                    os.path.join(os.path.abspath("tests/data"), "random_files", "file1"),
                    os.path.join(os.path.abspath("tests/data"), "random_files", "subdir2", "file3"),
                ],
                "random_files",
                streamed_archive_output_file
            )
            streamed_archive_from_disk = zipfile.ZipFile(streamed_archive_output_file.name)
            assert streamed_archive_from_disk.namelist() == [
                'random_files/file1',
                'random_files/file3',
            ]
