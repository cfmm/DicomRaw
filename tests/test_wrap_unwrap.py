import glob
import os
from tempfile import NamedTemporaryFile
import unittest
from parameterized import parameterized

from datetime import datetime
import time

import pydicom
from io import BytesIO
from pydicom.dataset import Dataset, FileDataset
from pydicom.filereader import read_file_meta_info
from pydicom.uid import PYDICOM_IMPLEMENTATION_UID, generate_uid
from pydicom.util.leanread import ImplicitVRLittleEndian, ExplicitVRLittleEndian, ExplicitVRBigEndian,\
    DeflatedExplicitVRLittleEndian
import dicomraw

import itertools

import subprocess


def transfer_syntax(uid):
    """Parse the transfer syntax

    Copied from pydicom pre-2.2.0 (Refactored out in subsequent versions)

    :return: is_implicit_vr, is_little_endian

    """
    # Assume a transfer syntax, correct it as necessary
    is_implicit_vr = True
    is_little_endian = True
    if uid == ImplicitVRLittleEndian:
        pass
    elif uid == ExplicitVRLittleEndian:
        is_implicit_vr = False
    elif uid == ExplicitVRBigEndian:
        is_implicit_vr = False
        is_little_endian = False
    elif uid == DeflatedExplicitVRLittleEndian:
        raise NotImplementedError("This reader does not handle deflate files")
    else:
        # PS 3.5-2008 A.4 (p63): other syntax (e.g all compressed)
        #    should be Explicit VR Little Endian,
        is_implicit_vr = False
    return is_implicit_vr, is_little_endian


def _process(fname):
    proc = subprocess.run(
        ["dciodvfy", fname],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    for line in proc.stderr.splitlines():
        yield line.decode('ascii').replace("\n", "")


def _determine(line):
    try:
        return "warnings", line[line.rindex("Warning - ") + 10:]
    except ValueError:
        try:
            return "errors", line[line.index("Error - ") + 8:]
        except ValueError:
            return None, None


def validate(fname):
    """ This function uses dciodvfy to generate
    a list of warnings and errors discovered within
    the DICOM file.
    :param fname: Location and filename of DICOM file.
    """
    validation = {
        "errors": [],
        "warnings": []
    }
    for line in _process(fname):
        kind, message = _determine(line)
        if kind in validation:
            validation[kind].append(message)
    return validation


private_group = 0x0177
content_creator = "Robarts^CFMM^dicomwrapper^0.0.1"


class DicomSetup(object):
    @staticmethod
    def get_file_meta(is_implicit_vr, is_little_endian, media_storage_sop_instance_uid=generate_uid()):
        raw_image_sop_class_uid = '1.2.840.10008.5.1.4.1.1.66'

        # Populate required values for file meta information
        file_meta = Dataset()
        file_meta.MediaStorageSOPClassUID = raw_image_sop_class_uid  # CT Image Storage
        file_meta.MediaStorageSOPInstanceUID = media_storage_sop_instance_uid
        file_meta.ImplementationClassUID = PYDICOM_IMPLEMENTATION_UID

        if is_implicit_vr:
            file_meta.TransferSyntaxUID = pydicom.uid.ImplicitVRLittleEndian
        else:
            if is_little_endian:
                file_meta.TransferSyntaxUID = pydicom.uid.ExplicitVRLittleEndian
            else:
                file_meta.TransferSyntaxUID = pydicom.uid.ExplicitVRBigEndian

        return file_meta

    @staticmethod
    def get_file_dataset(output_file_path, reference_dicom_dataset,
                         is_implicit_vr, is_little_endian,
                         save=True, media_storage_sop_instance_uid=generate_uid(), series_instance_uid=generate_uid()):

        ds = FileDataset(output_file_path, {},
                         file_meta=DicomSetup.get_file_meta(
                             is_implicit_vr,
                             is_little_endian,
                             media_storage_sop_instance_uid
                         )
                         )
        ds.is_implicit_VR = is_implicit_vr
        ds.is_little_endian = is_little_endian

        ds = DicomSetup.populate_defaults(ds, series_instance_uid)
        ds = DicomSetup.populate_header_from(ds, reference_dicom_dataset)
        if save:
            ds.save_as(ds.filename, write_like_original=False)
        return ds

    @staticmethod
    def populate_header_from(ds, reference_dicom_dataset):
        with open(next(glob.iglob(reference_dicom_dataset)), 'rb') as fp:
            ref_ds = pydicom.read_file(fp, stop_before_pixels=True)
            for elem in [
                # Patient Module
                "PatientName", "PatientID", "PatientBirthDate", "PatientSex",
                # General Study Module
                "StudyInstanceUID", "StudyDate", "StudyTime", "ReferringPhysicianName", "StudyID", "AccessionNumber",
                # General Equipment Module
                "Manufacturer",
                # Frame of Reference Module (optional, but validator requires it. Probably a bug in the validator
                # (dciodfvy)
                "FrameOfReferenceUID", "PositionReferenceIndicator"
            ]:
                setattr(ds, elem, getattr(ref_ds, elem))
        return ds

    @staticmethod
    def populate_defaults(ds, series_instance_uid=generate_uid()):
        ds.SeriesInstanceUID = series_instance_uid
        ds.Modality = "OT"  # Other
        ds.SeriesNumber = 99
        ds.Laterality = ""

        ds.AcquisitionContextSequence = ""

        ds.InstanceNumber = 99
        now = datetime.fromtimestamp(time.time())
        ds.ContentDate = now.strftime('%Y%m%d')
        ds.ContentTime = now.strftime('%H%M%S.%f')
        ds.CreatorVersionUID = ds.file_meta.ImplementationClassUID  # for now

        ds.SOPClassUID = '1.2.840.10008.5.1.4.1.1.66'
        ds.SOPInstanceUID = ds.file_meta.MediaStorageSOPInstanceUID
        return ds


class TestWrapUnwrap(unittest.TestCase):

    @staticmethod
    def wrap(dicom_file_path, bytestring, encapsulated_buffer_size, is_implicit_vr, is_little_endian):
        with open(dicom_file_path, 'ab') as fp:
            wrapper = dicomraw.Wrapper(
                BytesIO(bytestring),
                private_group=private_group,
                content_creator=content_creator,
                buffer_size=encapsulated_buffer_size,
                is_implicit_vr=is_implicit_vr,
                is_little_endian=is_little_endian
            )
            for chunk in wrapper:
                fp.write(chunk)

    @staticmethod
    def unwrap(dicom_file_path):
        meta_info = read_file_meta_info(dicom_file_path)
        is_implicit_vr, is_little_endian = transfer_syntax(meta_info.get("TransferSyntaxUID").encode())
        with open(dicom_file_path, 'rb') as input_fp:
            return b''.join([
                chunk for chunk in dicomraw.Unwrapper(
                    input_fp,
                    private_group,
                    is_implicit_vr,
                    is_little_endian
                )
            ])

    def wrap_and_unwrap(
            self, string_size, encapsulated_buffer_size, is_implicit_vr, is_little_endian,
            reference_dataset=os.path.abspath('tests/data/noise_phantom/*')
    ):
        temp_dir = os.path.abspath('tests/data/tmp')
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)
        with NamedTemporaryFile(dir=temp_dir) as dicom_output_file:
            wrapped_data = os.urandom(string_size)
            DicomSetup.get_file_dataset(dicom_output_file.name, reference_dataset, is_implicit_vr, is_little_endian,
                                        save=True)
            self.wrap(dicom_output_file.name, wrapped_data, encapsulated_buffer_size, is_implicit_vr, is_little_endian)

            unwrapped_data = self.unwrap(dicom_output_file.name)
            wrapped_dicom_dataset = pydicom.read_file(dicom_output_file.name)
            return wrapped_data, unwrapped_data, validate(dicom_output_file.name), wrapped_dicom_dataset

    @parameterized.expand(
        [x + (False, True) for x in itertools.product(
            [20, 21, 40, 41, 100, 101],
            [20, 21, 40, 41, 100, 101],
        )] + [(20, 40, True, True), (20, 40, False, False)],
        testcase_func_name=lambda testcase_func, param_num, param:
        "%s_stringsize_%d_buffersize_%d_implicitvr_%d_littleendian_%d" % (
                testcase_func.__name__, param[0][0], param[0][1], param[0][2], param[0][3])
    )
    def test_inputs(self, string_size, encapsulated_buffer_size, is_implicit_vr, is_little_endian):

        # call wrap_and_unwrap. If encapsulated_buffer_size is odd, expect a RuntimeError
        try:
            wrapped_data, unwrapped_data, validation, wrapped_dicom_dataset = \
                self.wrap_and_unwrap(string_size, encapsulated_buffer_size, is_implicit_vr, is_little_endian)
        except RuntimeError:
            if encapsulated_buffer_size % 2 == 1:
                return
            else:
                raise
        else:
            if encapsulated_buffer_size % 2 == 0:
                pass
            else:
                raise RuntimeError("Did not raise exception on odd encapsulated_buffer_size")

        unwrapped_manually = b''.join([sqel.EncapsulatedDocument for sqel in wrapped_dicom_dataset[0x0177, 0x1000]])
        assert(
            (
                string_size % encapsulated_buffer_size % 2 == 0 and len(unwrapped_manually) == len(wrapped_data)+2
            ) or
            (
                string_size % encapsulated_buffer_size % 2 == 1 and len(unwrapped_manually) == len(wrapped_data)+1
            )
        ), "Padding problem (overall length does not make sense)"
        assert unwrapped_manually[:-unwrapped_manually[-1]] == wrapped_data, \
            "Padding problem (data without padding does not match original)"

        assert wrapped_data == unwrapped_data, "sizes [{0}, {1}]: Stored and retrieved data don't match".format(
            string_size, encapsulated_buffer_size
        )
        assert len(validation['errors']) == 0, "sizes [{0}, {1}]: Errors during DICOM file validation: [{2}]".format(
            string_size, encapsulated_buffer_size, ','.join(validation['errors'])
        )
